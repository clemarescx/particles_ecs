
extern crate graphics;
extern crate piston_window;
extern crate rand;

use graphics::math::{add, mul_scalar, Vec2d};
use piston_window::{clear, rectangle, PistonWindow, WindowSettings};
use rand::distributions::{IndependentSample, Range};
use std::cell::RefCell;
use std::time::{Duration, SystemTime};


type RGBA = [f32; 4];
const WHITE: RGBA = [1.0; 4];
const GRAY: RGBA = [0.7, 0.7, 0.7, 0.3];
const N_PARTICLES: usize = 1000;
const MS_PER_UPDATE: u64 = 17;

const SCREEN_WIDTH: u32 = 640;
const SCREEN_HEIGHT: u32 = 480;
const SCREEN_CENTER: Vec2d = [SCREEN_WIDTH as f64 / 2f64, SCREEN_HEIGHT as f64 / 2f64];

struct World {
    current_turn: usize,
    positions: Vec<Vec2d>,
    sizes: Vec<Vec2d>,
    velocities: Vec<Vec2d>,
    accelerations: Vec<Vec2d>,
    colors: Vec<RGBA>,
}

impl World {
    fn new(width: u32, height: u32) -> World {
        World {
            current_turn: 0,
            sizes: Vec::with_capacity(N_PARTICLES),
            positions: Vec::with_capacity(N_PARTICLES),
            velocities: Vec::with_capacity(N_PARTICLES),
            accelerations: Vec::with_capacity(N_PARTICLES),
            colors: Vec::with_capacity(N_PARTICLES),
        }
    }

    fn add_shape(&mut self, x: f64, y: f64) {
        let mut rng = rand::thread_rng();
        let legal_range = Range::new(-5_f64, 5_f64);

        let x_speed = legal_range.ind_sample(&mut rng);
        let y_speed = legal_range.ind_sample(&mut rng);
        let x_accel = 0.1 * legal_range.ind_sample(&mut rng);
        let y_accel = 0.1 * legal_range.ind_sample(&mut rng);

        self.sizes.push([10.0, 10.0]);
        self.positions.push([x, y]);
        self.velocities.push([x_speed, y_speed]);
        self.accelerations.push([x_accel, y_accel]);
        self.colors.push(GRAY);
    }

    fn add_shapes(&mut self, n: usize) {
        let [x, y] = SCREEN_CENTER;

        for _ in 0..n {
            self.add_shape(x, y);
        }
    }

    fn remove_shape(&mut self) {
        self.sizes.remove(0);
        self.positions.remove(0);
        self.velocities.remove(0);
        self.accelerations.remove(0);
        self.colors.remove(0);
    }

    fn remove_shapes(&mut self, n: usize) {
        let n_shapes = self.positions.len();

        let to_remove = if n > n_shapes { n_shapes } else { n };

        for _ in 0..to_remove {
            self.remove_shape();
        }
    }

    fn update_velocities(&mut self) {
        self.velocities
            .iter_mut()
            .zip(&self.accelerations)
            .for_each(|(v, a)| *v = add(*v, *a));
    }
    fn update_positions(&mut self) {
        self.positions
            .iter_mut()
            .zip(&self.velocities)
            .for_each(|(p, v)| *p = add(*p, *v));
    }

    fn update_accelerations(&mut self) {
        self.accelerations
            .iter_mut()
            .for_each(|a| *a = mul_scalar(*a, 0.7));
    }
    fn update_colors(&mut self) {
        self.colors.iter_mut().for_each(|c| c[3] *= 0.97)
    }

    fn update(&mut self) {
        const N: f64 = N_PARTICLES as f64;
        const MAX: f64 = N * 0.5;
        const MIN: f64 = -N * 0.5;
        let x: f64 = self.current_turn as f64;

        let n = 0.4 * N * (0.1 * x).sin() + 0.1 * N * x.sin();
        let n = n.max(MIN).min(MAX).round() as isize;

        if n > 0 {
            self.add_shapes(n as usize);
        } else {
            self.remove_shapes(n.abs() as usize);
        }

        self.update_velocities();
        self.update_positions();
        self.update_accelerations();
        self.update_colors();

        self.current_turn += 1;
    }
}


fn main() {
    // let (width, height) = (640, 480);
    let mut window: PistonWindow =
        WindowSettings::new("particles_ecs", [SCREEN_WIDTH, SCREEN_HEIGHT])
            .exit_on_esc(true)
            .build()
            .expect("Could not create a window.");

    let mut world = World::new(SCREEN_WIDTH, SCREEN_HEIGHT);
    let mut world = RefCell::new(&mut world);
    world.get_mut().add_shapes(N_PARTICLES);

    let mut previous = SystemTime::now();
    let mut lag = Duration::new(0, 0);

    while let Some(event) = window.next() {
        let elapsed = previous.elapsed().unwrap_or_default();
        previous = SystemTime::now();
        lag += elapsed;

        while lag.as_millis() >= MS_PER_UPDATE.into() {
            world.get_mut().update();
            lag -= Duration::from_millis(MS_PER_UPDATE);
        }


        window.draw_2d(&event, |ctx, renderer, _| {
            clear(WHITE, renderer);
            let transformation_matrix = ctx.transform;
            world
                .borrow()
                .positions
                .iter()
                .zip(&world.borrow().sizes)
                .map(|(pos, size)| [pos[0], pos[1], size[0], size[1]])
                .zip(&world.borrow().colors)
                .for_each(|(rect, color)| rectangle(*color, rect, transformation_matrix, renderer));
        });
    }
}

